FROM alpine:3.15.4 as setup
ENV DELTA_PLUGIN_VERSION=0.21.0
ENV DELTA_PLUGIN_SHA=9fb01f2e40d3e301770b5d91c21d9a48d549bf1cb74c37d886254a3156744526
RUN apk add zip
RUN wget https://gitlab.com/bmwinger/delta-plugin/-/releases/$DELTA_PLUGIN_VERSION/downloads/delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip
RUN echo "$DELTA_PLUGIN_SHA  delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip" | sha256sum -c -
RUN unzip delta-plugin-$DELTA_PLUGIN_VERSION-linux-amd64.zip
RUN wget https://github.com/Greatness7/tes3conv/releases/download/v0.1.0/ubuntu-latest.zip && tar xf ubuntu-latest.zip
RUN echo "4f0540d33a92ae47b8131727b576f82eb221b3287d2138a0d556ee9791e8699b  tes3conv" | sha256sum -c -
COPY *.yaml build.sh /
RUN PATH=.:$PATH ./build.sh

FROM rust:1.67-alpine as habasifactory
RUN apk add libc-dev
RUN rustup install nightly
RUN wget https://github.com/alvazir/habasi/archive/refs/heads/master.zip && unzip master.zip
RUN cargo +nightly install --path habasi-master


FROM debian:12.4-slim
COPY --from=setup [ \
    "/Morrowind.esm", \
    "/Tribunal.esm", \
    "/Bloodmoon.esm", \
    "/Tamriel_Data.esm", \
    "/TR_Mainland.esm", \
    "/Cyr_Main.esm", \
    "/OAAB_Data.esm", \
    "/Dr_Data.esm", \
    "/Rise of House Telvanni.esm", \
    "/Solstheim Tomb of The Snow Prince.esm", \
    "/StarwindRemasteredV1.15.esm", \
    "/StarwindRemasteredPatch.esm", \
    "/Starwind Enhanced.esm", \
    "/AFFresh.esm", \
    "/Abandoned_Flat.esp", \
    "/Adanumuran Reclaimed.esp", \
    "/Aether Pirate\\'s Discovery.omwaddon", \
    "/BirthsignsRESPECted.esp", \
    "/Beautiful cities of Morrowind.ESP", \
    "/Blademeister_v1.5.esp", \
    "/The Doors of Oblivion 1.4.esp", \
    "/Draggle-Tail Shack.ESP", \
    "/Ebonheart_Underworks.esp", \
    "/HM_DDD_Strongholds_T_v1.0.esp", \
    "/Justice4Khartag+OAAB_Shipwreck - merged.ESP", \
    "/Kogoruhn - Extinct City of Ash and Sulfur.esp", \
    "/Nordic Dagon Fel.ESP", \
    "/Mamaea Awakened.ESP", \
    "/OAAB - Shipwrecks.ESP", \
    "/OAAB Brother Junipers Twin Lamps.esp", \
    "/Redaynia Restored.ESP", \
    "/Riharradroon - Path to Kogoruhn v1.0.ESP", \
    "/RR_Better_Ships_n_Boats_Eng.ESP", \
    "/RR_Better_Ships_n_Boats_Eng.esp", \
    "/Samarys Ancestral Tomb Expanded.esp", \
    "/Shipyards of Vvardenfell.esp", \
    "/The_corprusarium_Exp.ESP", \
    "/UL_3.5_RoHT_1.52_Add-on.esp", \
    "/UL_3.5_TR_21.01_Add-on.esp", \
    "/UL_3.5_TR_21.01_Add-on.omwaddon", \
    "/Uvirith\\'s Legacy_3.53.esp", \
    "/Uvirith\\'s Manor.ESP", \
    "/ghastly gg.esp", \
	"/Wares-base.esm", \
	"/Wares_containers.ESP", \
    "/srv/" \
]
COPY --from=setup /delta_plugin /usr/bin/
COPY --from=setup /tes3conv /usr/bin/
COPY --from=habasifactory /usr/local/cargo/bin/habasi /usr/bin/
RUN mkdir -p "$HOME"/.config/openmw && echo "data=\"/srv\"" > "$HOME"/.config/openmw/openmw.cfg
WORKDIR /src
