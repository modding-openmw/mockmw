VERSION = $(shell git describe --tags || echo 0)

all build:
	./build.sh

clean:
	rm -fr *.esm *.esp *.ESP *.omwaddon *.omwgame *.zip *.sha*sum.txt version.txt tmp

image: build
	docker build . -t modding-openmw/mw-mod-builder:$(VERSION)

Morrowind.esm:
	./build.sh Morrowind

Tribunal.esm:
	./build.sh Tribunal

Bloodmoon.esm:
	./build.sh Bloodmoon

Tamriel_Data.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh Tamriel_Data

TR_Mainland.esm: Tamriel_Data.esm
	./build.sh TR_Mainland

Cyr_Main.esm: Tamriel_Data.esm
	./build.sh Cyr_Main

Dr_Data.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh Dr_Data

OAAB_Data.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh OAAB_Data

AFFresh.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh AFFresh

BirthsignsRESPECted.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh BirthsignsRESPECted

The_corprusarium_Exp.ESP: Tamriel_Data.esm OAAB_Data.esm
	./build.sh The_corprusarium_Exp

Aether\ Pirate\'s\ Discovery.omwaddon: OAAB_Data.esm
	./build.sh "Aether Pirate's Discovery"

Ebonheart_Underworks.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh Ebonheart_Underworks

Samarys\ Ancestral\ Tomb\ Expanded.esp: Morrowind.esm
	./build.sh "Samarys Ancestral Tomb Expanded"

Adanumuran\ Reclaimed.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Adanumuran Reclaimed"

Mamaea\ Awakened.ESP: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Mamaea Awakened"

Blademeister_v1.5.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh Blademeister_v1.5

Shipyards\ of\ Vvardenfell.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Shipyards of Vvardenfell"

The\ Doors\ of\ Oblivion\ 1.4.esp:  Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "The Doors of Oblivion 1.4"

Draggle-Tail\ Shack.ESP: OAAB_Data.esm
	./build.sh "Draggle-Tail Shack"

Rise\ of\ House\ Telvanni.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Rise of House Telvanni"

Beautiful\ cities\ of\ Morrowind.ESP: Tamriel_Data.esm OAAB_Data.esm
	./build.sh "Beautiful cities of Morrowind"

Redaynia\ Restored.ESP: Tamriel_Data.esm
	./build.sh "Redaynia Restored"

Kogoruhn\ -\ Extinct\ City\ of\ Ash\ and\ Sulfur.esp: Riharradroon\ -\ Path\ to\ Kogoruhn\ v1.0.ESP Tamriel_Data.esm OAAB_Data.esm Dr_Data.esm
	./build.sh "Kogoruhn - Extinct City of Ash and Sulfur"

Riharradroon\ -\ Path\ to\ Kogoruhn\ v1.0.ESP: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Riharradroon - Path to Kogoruhn v1.0"

RR_Better_Ships_n_Boats_Eng.ESP: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh RR_Better_Ships_n_Boats_Eng

UL_3.5_TR_21.01_Add-on.omwaddon: TR_Mainland.esm
	./build.sh UL_3.5_TR_21.01_Add-on

ghastly\ gg.esp: Morrowind.esm
	./build.sh "ghastly gg"

OAAB\ -\ Shipwrecks.ESP: OAAB_Data.esm
	./build.sh "OAAB - Shipwrecks"

Nordic\ Dagon\ Fel.ESP: Tamriel_Data.esm OAAB_Data.esm
	./build.sh "Nordic Dagon Fel"

StarwindRemasteredV1.15.esm: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh StarwindRemasteredV1.15

StarwindRemasteredPatch.esm: StarwindRemasteredV1.15.esm
	./build.sh StarwindRemasteredPatch

Starwind\ Enhanced.esm: StarwindRemasteredPatch.esm
	./build.sh "Starwind Enhanced"

UL_3.5_RoHT_1.52_Add-on.esp: Rise\ of\ House\ Telvanni.esm
	./build.sh UL_3.5_RoHT_1.52_Add-on

Justice4Khartag+OAAB_Shipwreck\ -\ merged.ESP: Tamriel_Data.esm OAAB_Data.esm
	./build.sh "Justice4Khartag+OAAB_Shipwreck - merged"

Uvirith\'s\ Manor.ESP: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Uvirith's Manor"

HM_DDD_Strongholds_T_v1.0.esp: Morrowind.esm
	./build.sh HM_DDD_Strongholds_T_v1.0

Uvirith\'s\ Legacy_3.53.esp: Morrowind.esm Tribunal.esm Bloodmoon.esm
	./build.sh "Uvirith's Legacy_3.53"

Abandoned_Flat.esp: Morrowind.esm
	./build.sh Abandoned_Flat

Solstheim\ Tomb\ of\ The\ Snow\ Prince.esm: Tamriel_Data.esm
	./build.sh "Solstheim Tomb of The Snow Prince"

OAAB\ Brother\ Junipers\ Twin\ Lamps.esp: OAAB_Data.esm
	./build.sh "OAAB Brother Junipers Twin Lamps"

Wares\-base.esm: Wares-base.esm
	./build.sh "Wares-base"

Wares_containers.ESP: Wares_containers.ESP
	./build.sh "Wares_containers"
